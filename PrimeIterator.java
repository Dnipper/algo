import java.util.Iterator;


public class PrimeIterator implements Iterator<Integer> {
	boolean[] isPrime;
	int indexBool;
	int indexNum;
	public PrimeIterator(int max) {
		indexBool=0;
		indexNum=0;
		isPrime=new boolean[max+1];
		for(int i=2;i<=max;i++)
		{
			isPrime[i]=true;
		}
		
		for(int i=2;i*i<=max;i++)
		{
			if(isPrime[i])
			{
				for(int j=i;i*j <=max;j++)
				{
					isPrime[i*j]=false;
				}
			}
		}
	}
	/**
* Returns whether this iterator has any more prime numbers less than max
**/
	public boolean hasNext() { 
		
			while(!isPrime[indexBool] && indexBool<=isPrime.length-1 )
			{
				
				indexBool++;
				
			}
			
			if(indexBool<=isPrime.length-1)
			{
				return true;
			}
			else
			{
				return false;
			}
		
		
		
	}
	/**
* Gets the next prime number
**/
	
	public Integer next() 
	{ 
		while(!(indexNum<=isPrime.length-1 && isPrime[indexNum]))
		{
			if(indexNum<isPrime.length)
				indexNum++;
		}
		indexNum++;
		
		return (indexNum-1);
	}
	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	} 
}

