
public class AVLBasedPriorityQueue<T extends Comparable<T>>  {
private AVLNode root=null;
public int size=0;

private boolean reBalanced;
	boolean offer(T data) 
	{
		AVLNode temp=new AVLNode();
		temp.data=data;
		if(root==null)
		{
			root=temp;
		}
		else
		{
			insert(root,temp);
		}
		reBalanced=true;
		while(reBalanced)
		{
			reBalanced=false;
			balance(root,null);
		}
		size++;
		return true;
	}
	public void insert(AVLNode inNode,AVLNode toBeInserted)
	{
		if(inNode.left==null && (inNode.compareTo(toBeInserted)>0))
		{
			inNode.left=toBeInserted;
		}
		else if(inNode.right==null && (inNode.compareTo(toBeInserted)<0))
		{
			inNode.right=toBeInserted;
		}
		else if((inNode.compareTo(toBeInserted)<0))
		{
			insert(inNode.right,toBeInserted);
		}
		else if((inNode.compareTo(toBeInserted)>0))
		{
			insert(inNode.left,toBeInserted);
		}
	}
	T peek() {
		if(size==0)
		{
			throw new IllegalArgumentException("AVL cannot be empty");
		}
		return (T) (findHighest(root)).data;
	}
	T poll()
	{
		if(size==0)
		{
			throw new IllegalArgumentException("AVL cannot be empty");
		}
		AVLNode temp=new AVLNode();
		temp=findHighestPoll(root,null);
		if(root!=null)
		{
			reBalanced=true;
			while(reBalanced)
			{
				reBalanced=false;
				balance(root,null);
			}
		}
		size--;
		return (T) temp.data;
	}
	public AVLNode findHighestPoll(AVLNode inNode,AVLNode parent)
	{
		
		AVLNode temp=new AVLNode();
		if(inNode.left==null)
		{
			temp.data=inNode.data;
			if(parent!=null && inNode.right==null)
			{
				parent.left=null;
			}
			else if(inNode.right!=null && parent!=null)
			{
				parent.left=inNode.right;
			}
			else if(inNode.right!=null && parent==null)
			{
				root=inNode.right;
			}
			else
			{
				root=null;
			}
			
			return temp;
		}
		else
		{
			return findHighestPoll(inNode.left,inNode);
		}
	}
	public AVLNode findHighest(AVLNode inNode)
	{
		
		
		if(inNode.right==null)
		{
			return inNode;
		}
		else
		{
			return findHighest(inNode.right);
		}
	}
	private void balance(AVLNode inNode, AVLNode parent)
	{
		if(inNode==null)
		{
			return;
		}
		
		balance(inNode.left,inNode);
		balance(inNode.right,inNode);
		int result=(balanceHelper(inNode.left)-balanceHelper(inNode.right));
		if(result>=2)
		{
			reBalanced=true;
			reBalance(inNode,parent);
		}
		else if(result<=-2)
		{
			reBalanced=true;
			reBalance(inNode,parent);
		}
		
	}
	
	private int balanceHelper(AVLNode inNode)
	{
		int left=0;
		int right=0;
		if(inNode==null)
		{
			return 0;
		}
		if(inNode.left==null && inNode.right==null)
		{
			return 1;
		}
		
		if(inNode.left!=null)
		{
			left=balanceHelper(inNode.left)+1;
		}
		if(inNode.right!=null)
		{
			right=balanceHelper(inNode.right)+1;
		}
		
		return Math.max(left, right);
		

		
	}
	private int balanceFactor(AVLNode inNode)
	{
		return (balanceHelper(inNode.left)-balanceHelper(inNode.right));
	}
	private void right(AVLNode inNode,AVLNode parent)
	{
		AVLNode pivot= new AVLNode();
		pivot=inNode.left;
		inNode.left=pivot.right;
		pivot.right=inNode;
		if(parent==null)
		{
			root=pivot;
		}
		else if(parent.right==inNode)
		{
			parent.right=pivot;
		}
		else
		{
			parent.left=pivot;
		}
	}
	private void left(AVLNode inNode,AVLNode parent)
	{
		AVLNode pivot= new AVLNode();
		pivot=inNode.right;
		inNode.right=pivot.left;
		pivot.left=inNode;
		if(parent==null)
		{
			root=pivot;
		}
		else if(parent.right==inNode)
		{
			parent.right=pivot;
		}
		else
		{
			parent.left=pivot;
		}
	}
	private void reBalance(AVLNode inNode,AVLNode parent)
	{
		
		if(balanceFactor(inNode)>=2)
		{
			if(balanceFactor(inNode.left)>0)
			{
				right(inNode,parent);
			}
			else
			{
				left(inNode.left,inNode);
				right(inNode,parent);
			}
		}
		if(balanceFactor(inNode)<=-2)
		{
			if(balanceFactor(inNode.right)>0)
			{
				right(inNode.right,inNode);
				left(inNode,parent);
			}
			else
			{
				
				left(inNode,parent);
			}
		}
	}
	
	
	public void printTree()
	{
		printTreeHelper(root, null);
	}
	private void printTreeHelper(AVLNode inNode, AVLNode parent)
	{
		if(inNode==null)
		{
			return;
		}
		printTreeHelper(inNode.left,inNode);
		printTreeHelper(inNode.right,inNode);
		if(parent!=null)
		{
			System.out.println("Parent: " + parent.data + ", currentNode: " + inNode.data+ ", Height: " +(balanceHelper(inNode.left)-balanceHelper(inNode.right) ));
		}
		else
		{
			System.out.println("root " + ", currentNode: " + inNode.data+ ", Height: " +(balanceHelper(inNode.left)-balanceHelper(inNode.right) ));
		}
	}
}
