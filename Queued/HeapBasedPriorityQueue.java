import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class HeapBasedPriorityQueue<T extends Comparable<T>> {
	List<T> heapList=new ArrayList<T>();
	private int currentSize;
	public HeapBasedPriorityQueue(int initialSize) {
		
		heapList.add(null);
		currentSize=0;
	}
	public boolean offer(T data)
	{
		heapList.add(data);
		currentSize++;
		heapify();
		
		return true;
		
	}
	
	public T peek()
	{
		if(currentSize>0)
		{
			return heapList.get(1);
		}
		else 
		{
			throw new IllegalArgumentException("Heap cannot be empty");
		}
		
	}
	
	public T poll()
	{
		T temp;
		if(currentSize>0 &&currentSize>1 )
		{
			Collections.swap(heapList, 1, currentSize);
			temp=heapList.remove(currentSize);
			currentSize--;
			heapify();
			
		}
		else if(currentSize==1)
		{
			temp=heapList.remove(currentSize);
			currentSize--;
		}
		else 
		{
			throw new IllegalArgumentException("Heap cannot be empty");
		}
		return temp;
	}
	private void heapify()
	{
		boolean isBalanced=false;
		boolean noChanges=true;
		int start=currentSize;
		while(!isBalanced)
		{
			
			if(start/2>0)
			{
				if((heapList.get(start).compareTo(heapList.get(start/2)))<0)
				{
					Collections.swap(heapList, start, start/2);
					noChanges=false;
					
				}
				start--;
						
			}
			else if(noChanges)
			{
				isBalanced=true;
			}
			else
			{
				noChanges=true;
				start=currentSize;
			}
		}
	}


}
