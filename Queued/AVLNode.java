
public class AVLNode<T extends Comparable> implements Comparable<AVLNode> {

	public T data;
	public AVLNode left;
	public AVLNode right;

	@Override
	public int compareTo(AVLNode inNode) {
		// TODO Auto-generated method stub
		int result=0;
		
		result=this.data.compareTo(inNode.data);
		return result;
	}

}
