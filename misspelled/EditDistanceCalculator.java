
public class EditDistanceCalculator {
	public int editDistance(String a, String b) {
		return editDistanceHelper2(a, b, new int[a.length()+1][b.length()+1]);
	}
	
	private int editDistanceHelper(String a, String b, int[][] eds) {
		for ( int i = 1; i <= a.length(); i++ ) {
			eds[i][0] = b.length();
		}
		for ( int j = 1; j <= b.length(); j++ ) {
			eds[0][j] = a.length();
		}
		
		for ( int i = 1; i <= a.length(); i++ ) {
			for ( int j = 1;  j <= b.length(); j++ ) {
				int delete = eds[i-1][j] + 1;
				int insert = eds[i][j-1] + 1;
				int remove = eds[i-1][j-1] + (a.charAt(i-1) == b.charAt(j-1) ? 0 : 1);
				eds[i][j] = Math.min(delete, Math.min(insert, remove));
			}
		}
		
		return eds[a.length()][b.length()];
	}
	
	/**
	 * #2.  Add a recursive version here, which uses a memory function.  Change your internal implementation
	 * to use this version. (1 point)
	 */
	private int editDistanceHelper2(String a, String b, int[][] eds) {
		for ( int i = 1; i <= a.length(); i++ ) {
			eds[i][0] = b.length();
		}
		for ( int j = 1; j <= b.length(); j++ ) {
			eds[0][j] = a.length();
		}
		
		int[][][] values=new int[a.length()+b.length()][a.length()+b.length()][a.length()+b.length()];
		editDistanceHelper2Helper(a, b,1, 1,eds,values );
		return eds[a.length()][b.length()];
	}
	
	
	
		private void editDistanceHelper2Helper(String a, String b,int i, int j,int[][] eds,int[][][] values )
		{
			if(i<=a.length())
			{
				if(j<=b.length())
				{
					
					int delete = eds[i-1][j] + 1;
					int insert = eds[i][j-1] + 1;
					int remove = eds[i-1][j-1] + (a.charAt(i-1) == b.charAt(j-1) ? 0 : 1);
					
					if(values[delete][insert][remove]!=0)
					{
						eds[i][j] =values[delete][insert][remove];
					}
					else
					{
						values[delete][insert][remove]=Math.min(delete, Math.min(insert, remove));
						eds[i][j] = values[delete][insert][remove];
					}
					editDistanceHelper2Helper(a, b,i, ++j,eds,values);
				}
				else
				{
					i++;
					j=1;
					editDistanceHelper2Helper(a, b,i, j,eds,values);
				}
			}
		}
}
