

import java.util.Iterator;



public class Hashtable<K, V> {
	private Entry<K,V>[] values;
	private int size;
	
	public Hashtable(int initialCapacity) {
		values = (Entry<K,V>[])new Entry[initialCapacity];
	}
	
	/**
	 * #3b. Implement this (1 point)
	 * 
	 * @param key
	 * @param value
	 */
	public void put(K key, V value) {
		int hash=key.hashCode();
		Entry<K,V> temp=new Entry(key,value);
		int location=hash%values.length;
		if(values[location]==null)
		{
			values[location]=temp;
			size++;
		}
		else
		{
			Entry<K,V> temp2=values[location];
			boolean dataWasreplaced=false;
			while(temp2.next!=null&&!dataWasreplaced)
			{
				if(temp2.key==temp.key)
				{
					dataWasreplaced=true;
					temp2.data=temp.data;
				}
				temp2=temp2.next;
			}
			if(!dataWasreplaced)
			{
				temp2.next=temp;
			}
		}
		
	}
	
	/**
	 * #3b. Implement this (1 point)
	 * @param key
	 * @return
	 */
	public V get(K key) {
		int hash=key.hashCode();
		int location=hash%values.length;
		V returnData=null;
		Entry<K,V> temp2=values[location];
		if(temp2!=null)
		{
			if(temp2.key==key)
			{
				returnData=temp2.data;
			}
			else
			{
				while(temp2.next!=null)
				{
					if(temp2.key==key)
					{
						returnData=temp2.data;
						
					}
					temp2=temp2.next;
				}
			}
		}
		return returnData;
	}

	/**
	 * #3c.  Implement this. (1 point)
	 * 
	 * @param key
	 * @return
	 */
	public V remove(K key) {
		int hash=key.hashCode();
		int location=hash%values.length;
		V returnData=null;
		Entry<K,V> temp2=values[location];
		Entry<K,V> previous=null;
		if(temp2!=null)
		{
			if(temp2.key==key)
			{
				returnData=temp2.data;
				values[location]=temp2.next;
			}
			else
			{
				while(temp2.next!=null)
				{
					if(temp2.key==key)
					{
						returnData=temp2.data;
						if(previous !=null)
						{
							previous.next=temp2.next;
						}
						
						
					}
					previous=temp2;
					temp2=temp2.next;
				}
			}
		}
		if(returnData!=null)
		{
			size--;
		}
		return returnData;
	}
	
	public int size() {
		return size;
	}
	
	public boolean containsKey(K key) {
		return this.get(key) != null; 
	}

	public Iterator<V> values() {
		return new Iterator<V>() {
			private int count = 0;
			private Entry<K, V> currentEntry;
			
			{
				while ( ( currentEntry = values[count] ) == null && count < values.length ) {
					count++;
				}
			}
			
			

			@Override
			public boolean hasNext() {
				return count < values.length;
			}

			@Override
			public V next() {
				V toReturn = currentEntry.data;
				currentEntry = currentEntry.next;
				while ( currentEntry == null && ++count < values.length && (currentEntry = values[count]) == null );
				return toReturn;
			}

			@Override
			public void remove() {
			}
			
		};
	}
	
	private static class Entry<K, V> {
		private K key;
		private V data;
		private Entry<K,V> next;
		
		public Entry(K key, V data) {
			this.key = key;
			this.data = data;
		}
		
		public String toString() {
			return "{" + key + "=" + data + "}";
		}
	}
}