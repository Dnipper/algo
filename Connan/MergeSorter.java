import java.util.ArrayList;
import java.util.List;

import edu.neumont.csc250.lab4.Sorter;


public class MergeSorter<T extends Comparable<T>> implements Sorter<T> {

	@Override
	public void sort(List<T> toBeSorted) {
		
		 mergesort(toBeSorted, 0,toBeSorted.size());
	}
	private void mergesort(List<T> inArray, int low,int high)
	{
		int n;
		int n2;
		if(high>1)
		{
			n=high/2;
			n2=high-n;
			mergesort(inArray,low,n);
			mergesort(inArray,low + n,n2);
			
			merge(inArray, low, n, n2);
		}
		
	}
	private void merge(List<T> data, int low, int n, int n2)
	{
		List<T> temp=new ArrayList<T>();
		int copy1 =0;
		int copy2 =0;
		int copy3 =0;
		int index;
		
		while((copy2<n)&&(copy3<n2))
		{
			if((data.get(low+copy2).compareTo(data.get(low+n+copy3)))<0)
			{
				temp.add(copy1++, data.get(low+(copy2++)));
			}
			else
			{
				temp.add(copy1++, data.get(low+n+(copy3++)));
			}
			
		}
			while(copy2<n)
			{
				temp.add(copy1++, data.get(low+(copy2++)));
			}
			while(copy3<n2)
			{
				temp.add(copy1++, data.get(low+n+(copy3++)));
			}
			for(int i=0; i<n+n2;i++)
			{
				data.set(low+i, temp.get(i));
			}
		
	}
	

}
