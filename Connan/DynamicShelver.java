import java.util.ArrayList;
import java.util.List;

import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;
import edu.neumont.csc250.lab4.Shelver;


public class DynamicShelver implements Shelver {

	@Override
	public void shelveBooks(Bookcase bookcase, List<Book> books) {
		sortHighestToLowest(books);
		solveWordWrap(bookcase, books,books.size(),bookcase.getShelfWidth() );
		
	}
	public void solveWordWrap(Bookcase bookcase, List<Book> books,int n,int m )
	{
		int[][] extras= new int[n+1][n+1];
		int[][] lc= new int[n+1][n+1];
		int[] c=new int[n+1];
		int[] p=new int[n+1];
		
		for(int i=1;i<=n;i++)
		{
			extras[i][i]=m-books.get(i-1).getWidth();
			for(int j=i+1;j<=n;j++)
			{
				extras[i][j]=extras[i][j-1]-books.get(j-1).getWidth()-1;
			}
		}
		for(int i=1;i<=n;i++)
		{
			for(int j=i;j<=n;j++)
			{
				if(extras[i][j]<0)
				{
					lc[i][j]=10000;
				}
				else if(j==n && extras[i][j]>=0)
				{
					lc[i][j]=0;
				}
				else
				{
					lc[i][j]=extras[i][j]*extras[i][j];
				}
			}
		}
		c[0]=0;
		for(int j=1;j<=n;j++)
		{
			c[j]=10000;
			for(int i=1;i<=j;i++)
			{
				if(c[i-1] != 10000 && lc[i][j] != 10000 && (c[i-1] + lc[i][j] < c[j]))
				{
					c[j]=c[i-1]+lc[i][j];
					p[j]=i;
				}
			}
		}
		printSolution(p,n,bookcase,books);
	}
	private int printSolution(int p[],int n,Bookcase bookcase,List<Book> books)
	{
		 int k;
		    if (p[n] == 1)
		        k = 1;
		    else
		        k = printSolution (p, p[n]-1,bookcase,books) + 1;
		    int newK=k-1;
		    int firstOne=p[n]-1;
		    int secondOne=n-1;
		    for(int i=firstOne;i<n;i++)
		    {
		    	bookcase.addBook(newK,books.get(i));
		    }
		    
		   // System.out.println(bookcase.scoreShelfEvenness());
		    System.out.println("Shelf number "+newK+": "+ "From book no. " +firstOne+" to " + secondOne );
		    return k;
	}
	public  <T> void sortHighestToLowest(List<T> inArray)
	{
		List<T> temp =new ArrayList<T>();
		int index=0;
		for(int i=inArray.size()-1;i>=0;i--)
		{
			temp.add(inArray.get(i));
		}
		for(int i=0;i<temp.size();i++)
		{
			inArray.set(i, temp.get(i));
		}
	}

}
