import java.util.List;

import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;
import edu.neumont.csc250.lab4.Shelver;


public class GreedyShelver implements Shelver  {

	@Override
	public void shelveBooks(Bookcase bookcase, List<Book> books) {
		MergeSorter ms= new MergeSorter();
		ms.sort(books);
		int shelf=0;
		int booksOnshelf=0;
		bookcase.getNumberOfShelves();
		for(int i=books.size()-1;i>=0 && shelf<bookcase.getNumberOfShelves() ;i--)
		{
			if(bookcase.getShelfWidth()>booksOnshelf+books.get(i).getWidth())
			{
				bookcase.addBook(shelf, books.get(i));
				booksOnshelf+=books.get(i).getWidth();
			}
			
			else
			{
				shelf++;
				booksOnshelf=0;
				if(shelf<bookcase.getNumberOfShelves())
				{
					bookcase.addBook(shelf, books.get(i));
					booksOnshelf+=books.get(i).getWidth();
				}
				
			}
		}
		
	}

}
