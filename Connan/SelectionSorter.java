import java.util.List;

import edu.neumont.csc250.lab4.Sorter;


public class SelectionSorter<T extends Comparable<T>> implements Sorter<T> {

	@Override
	public void sort(List<T> toBeSorted) {
		int min;
		for(int j=0;j<toBeSorted.size()-1;j++)
		{
			min=j;
			for(int i=j+1;i<toBeSorted.size();i++)
			{
				if((toBeSorted.get(i).compareTo(toBeSorted.get(min)))<0)
				{
					min=i;
				}
			}
			T temp=toBeSorted.get(j);
			toBeSorted.set(j, toBeSorted.get(min)); 
			toBeSorted.set(min,temp);
		}
		
	}

}
