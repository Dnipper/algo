import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import edu.neumont.nlp.DecodingDictionary;


public class ExhaustiveDecoder {
	int max;
	List<Top20> meTop20=new ArrayList<Top20>();
	DecodingDictionary myDictionary;
	ExhaustiveDecoder(DecodingDictionary dd, int numWanted)
	{
		max=numWanted;
		myDictionary=dd;
	}
	public List<String> decode(String message)
	{ 
		List<String> theMessage = new ArrayList();
		String soFar="";
		
		decodeHelper(soFar, message);
	Collections.sort(meTop20);
	for(int x=0;x<20 && x<meTop20.size() ;x++)
	{
		theMessage.add(meTop20.get(x).toString());
	}

		
		
		return theMessage;
		
	}
	private void decodeHelper(String soFar,String toGo)
	{
		float result=0.0f;
		
		
	
				if(toGo.isEmpty())
				{
					result=dealWithFrequencies(soFar);
					Top20 top20Temp=new Top20(soFar,result);
					meTop20.add(top20Temp);
					
				}
		
			else
			{
					
					for(int i=0;i<=toGo.length();i++)
					{
						
						
						Set<String> test= myDictionary.getWordsForCode(toGo.substring(0, i));
						
						
						
						if(test!=null)
						{
							
							String newTogo="";
							String newSoFar="";
					
							newTogo=toGo.substring(i);
								for(String x: test)
								{	
									newSoFar = soFar + x + " ";
									result=dealWithFrequencies(newSoFar);
									if(result > .0001f)
									{
										
										decodeHelper(newSoFar,newTogo);
									}
								}
					
						}
					}
			}
		
	}
	float dealWithFrequencies(String comparer)
	{
		String[] temp=comparer.split(" ");
		String temp3="";
		float freq=1.0f;
		for(String temp2 : temp )
		{
			if(temp3.isEmpty())
			{
				temp3=temp2;
			}
			else
			{
				freq*=myDictionary.frequencyOfFollowingWord(temp3, temp2)/10000.0f;
				temp3=temp2;
			}
			
		}
		return freq;
	}

}
