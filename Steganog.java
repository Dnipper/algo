import java.awt.Color;
import java.io.IOException;

import edu.neumont.ui.Picture;


public class Steganog {

	public Picture embedIntoImage(Picture cleanImage, String message) throws IOException 
	{
		
		int theMessageCharacter=0;
		cleanImage.setOriginUpperLeft();
		int x=0;
		int y=0;
		int myMask=0x03;
		PrimeIterator prime=new PrimeIterator(cleanImage.width()*cleanImage.height());
		int tempPrime=prime.next();
		while(theMessageCharacter<message.length())
		{
			
			int charAskii=message.charAt(theMessageCharacter)-32;
			if(x>=cleanImage.width())
			{
				y++;
				x=0;
			}
			y=tempPrime/cleanImage.width();
			x=tempPrime-cleanImage.width()*y;
				

				tempPrime=prime.next();
				Color temp;
				temp=cleanImage.get(x, y);
				//red
				int valueToRemoveRed=temp.getRed() & myMask;
				int tempRed=temp.getRed()-valueToRemoveRed;
				//green
				int valueToRemoveGreen=temp.getGreen() & myMask;
				int tempGreen=temp.getGreen()-valueToRemoveGreen;
				//blue
				int valueToRemoveBlue=temp.getBlue() & myMask;
				int tempBlue=temp.getBlue()-valueToRemoveBlue;
				
				int redChar=charAskii/16;
				
				charAskii-=16*redChar;
				
				int greenChar=charAskii/4;
				charAskii-=4*greenChar;
				
				int blueChar=charAskii;
				temp=new Color(tempRed+redChar,tempGreen+greenChar,tempBlue+blueChar);
				cleanImage.set(x, y, temp);
				
				x++;
				theMessageCharacter++;
		}
		
		return cleanImage;
	
		
	}
	public String retreiveFromImage(Picture imageWithSecretMessage) throws IOException 
	{
		String returnString="";
		imageWithSecretMessage.setOriginUpperLeft();
		PrimeIterator prime=new PrimeIterator(imageWithSecretMessage.width()*imageWithSecretMessage.height());
		int tempPrime=prime.next();
		int tempY=0;
		int tempX=0;
		for(int y=0;y<imageWithSecretMessage.height();y++)
		{
			for(int x=0;x<imageWithSecretMessage.width();x++)
			{
				
				y=tempPrime/imageWithSecretMessage.width();
				x=tempPrime-imageWithSecretMessage.width()*y;
					

						tempPrime=prime.next();
						char tempChar=getChar(x, y ,imageWithSecretMessage);
						if(tempChar=='%')
						{
							y=imageWithSecretMessage.height();
							x=imageWithSecretMessage.width();
						}
						else
						{
							returnString+=tempChar; 
						}
					
				
				
			}
		}
		
		return returnString;
		
	}
	private char getChar(int x, int y ,Picture imageWithSecretMessage)
	{
		Color temp=imageWithSecretMessage.get(x, y);
		String red =Integer.toBinaryString(temp.getRed());
		String green= Integer.toBinaryString(temp.getGreen());
		String blue=Integer.toBinaryString(temp.getBlue());
		String myLetter;
		String redEnd = "";
		String greenEnd="";
		String blueEnd="";
		if(red.length()<=1)
		{
			redEnd=red;
		}
		else
		{
			redEnd=red.substring(red.length()-2, red.length());
		}
		if(green.length()<=1)
		{
			greenEnd=green;
		}
		else
		{
			greenEnd=green.substring(green.length()-2, green.length());
		}
		if( blue.length()<=1)
		{
			blueEnd=blue;
		}
		else
		{
			blueEnd=blue.substring(blue.length()-2, blue.length());
		}
		blueEnd=ifZero(blueEnd);
		redEnd=ifZero(redEnd);
		greenEnd=ifZero(greenEnd);
			myLetter= redEnd+greenEnd +blueEnd;
		
		int almostAskii=Integer.parseInt(myLetter, 2);
		almostAskii+=32;
		return (char)almostAskii;
	}
	private String ifZero(String sub)
	{
		String temp=sub;
		if(sub.length()==1)
		{
			temp="0"+sub;
		}
		return temp;
			
	}
}

