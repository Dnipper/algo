import java.util.Comparator;


public class Top20 implements Comparable<Top20> {

	String myString;
	float myFloat;
	
	public Top20(String inString, float inFloat)
	{
		myString=inString;
		myFloat=inFloat;
	}
	
	@Override
	public int compareTo(Top20 inTop20) {
		int result=0;
		
		if(inTop20.myFloat>this.myFloat)
		{
			result=1;
		}
		else if(inTop20.myFloat<this.myFloat)
		{
			result=-1;
		}
		
		return result;
	}
	@Override
	public String toString()
	{
		return myString + " Frequency: "+ " "+ myFloat;
		
	}


}
