
public class Graph {
	private int[][] matrix;
	private int[] marks;
	private int edgeCount;
	public Graph(int n)
	{
		matrix=new int[n][n];
		marks=new int[n];
		
	}
	public int vCount()
	{
		return marks.length;
	}
	public int first(int vert)
	{
		for(int i=0;i<vCount();i++)
		{
			if(matrix[vert][i]!=0)
			{
				return i;
			}
			
		}
		return vCount();
		
	}
	public int next(int startVert,int afterVert)
	{
		for(int i=afterVert+1;i< vCount();i++)
		{
			if(matrix[startVert][i]!=0)
			{
				return i;
			}
		}
		return vCount();
		
	}
	public int numEdges(int vert)
	{
		int count=0;
		for(int i=0;i< vCount();i++)
		{
			if(matrix[vert][i]!=0)
			{
				count++;
			}
		}
		return count;
	}
	public void addEdge(int vertex, int neighbor,int weight)
	{
		matrix[vertex][neighbor]=weight;
		matrix[neighbor][vertex]=weight;
		edgeCount+=2;
	}
	public int ecount()
	{
		return edgeCount;
	}
	public int degree(int vertex)
	{
		int degreeOfVertex=0;
		for(int i=0;i<vCount();i++)
		{
			if(matrix[vertex][i]!=0)
			{
				degreeOfVertex++;
			}
		}
		return degreeOfVertex;
	}
	public void removeEdge(int vertex, int neighbor)
	{
		matrix[vertex][neighbor]=0;
		matrix[neighbor][vertex]=0;
		edgeCount-=2;
		
	}
	public boolean isEdge(int vertex, int neighbor)
	{
		return matrix[vertex][neighbor]!=0;
	}
	public int getMark(int v)
	{
		return marks[v];
	}
	public void setMark(int vertex,int mark)
	{
		marks[vertex]=mark;
	}
	public void resetMarks()
	{
		for(int i=0;i<vCount();i++)
		{
			marks[i]=0;
		}
	}
	

}
