import java.util.ArrayList;
import java.util.List;


public class DotsAndBoxes {
	private int numRows;
	private int numCol;
	int chainCount=0;
	public Graph g;
	private int[] players=new int[2];
	private BfsGraphTraversal bg=new BfsGraphTraversal();
	DotsAndBoxes(int rows, int columns)
	{
		players[0]=0;
		players[1]=0;
		numRows=rows+1;
		numCol=columns+1;
		int graphSize=(rows+1)*(columns+1);
		 g=new Graph(graphSize);
		for(int i=1;i<g.vCount();i++)
		{
			if(checkIfStringAllowed(i,rows,columns,graphSize))
			{
				if(i%(rows+1)==0)
				{
					g.addEdge(i, i+1, 1);
				}
				else if( i<numRows-1)
				{
					g.addEdge(i,i+(rows+1) , 1);
				}
				else
				{
					g.addEdge(i, i+1, 1);
					g.addEdge(i, i+(rows+1), 1);
				}
				
			}
			
		}
	}
	private boolean checkIfStringAllowed(int x,int rows,int columns,int graphSize)
	{
		boolean isNotEdge=false;
		if(x!=rows && !(x<=graphSize-1 && x>=((rows+1)*columns)) )
		{
			isNotEdge= true;
		}
		else
		{
			isNotEdge= false;
		}
		int edge=rows;
		while(edge<graphSize)
		{
			if(x==edge)
			{
				isNotEdge=false;
				edge=graphSize;
			}
			edge+=(rows+1);
			
		}
		return isNotEdge;
	}
	
	int drawLine(int player, int x1, int y1, int x2, int y2)
	{
		int score=0;
		if(x1-x2!=0)
		{
			int cutNum=numRows*y1;
			if(x1>x2)
			{
				cutNum+=x1;
			}
			else
			{
				cutNum+=x2;
			}
			if(g.isEdge(cutNum, cutNum+numRows))
			{
				g.removeEdge(cutNum, cutNum+numRows);
			//	System.out.println("StartEdge:" + cutNum + ": EndEdge: "+(cutNum+numRows) );
				if(isNotEdgeVerteg(cutNum))
				{
					if(g.numEdges(cutNum)==0)
					{
						score++;
					}
				}
				if(isNotEdgeVerteg(cutNum+numRows))
				{
					if(g.numEdges(cutNum+numRows)==0)
					{
						score++;
					}
				}
				
			}
		}
		else
		{
			int cutNum=numRows;
			if(y1>y2)
			{
				cutNum*=y1;
			}
			else
			{
				cutNum*=y2;
			}
			cutNum+=x1;
			if(g.isEdge(cutNum, cutNum+1))
			{
				g.removeEdge(cutNum, cutNum+1);
			//System.out.println("StartEdge:" + cutNum + ": EndEdge: "+(cutNum+1) );
				if(isNotEdgeVerteg(cutNum))
				{
					if(g.numEdges(cutNum)==0)
					{
						score++;
					}
				}
				if(isNotEdgeVerteg(cutNum+1))
				{
					if(g.numEdges(cutNum+1)==0)
					{
						score++;
					}
				}
				
			}
			
		}
		players[player]+=score;
		return score;
	}
	int score(int player)
	{
		return players[player];
	}
	boolean areMovesLeft()
	{
		boolean returnBoolean=true;
		List total=bg.traverse(g);
		if(total.size()==g.vCount())
		{
			returnBoolean=false;
		}
		return returnBoolean;
	}
	int countDoubleCrosses()
	{
		int count=0;
		List numDouble=bg.traverse(g);
		for(int i=0;i<numDouble.size();i++)
		{
			List temp=(List) numDouble.get(i);
			if(temp.size()==2)
			{
				if(isNotEdgeString((int)temp.get(0),(int)temp.get(1)))
				{
					count++;
				}
			}
		}
		return count;
	}
	private boolean isNotEdgeVerteg(int vert)
	{
		boolean isDoubleCross=false;
		int graphSize=numRows*numCol;
		
			if(vert!=numRows-1 && !(vert<=graphSize-1 && vert>=((numRows)*(numCol-1)))&& !(vert%(numRows)==0) && !(vert<numRows-1) )
			{
				
				
				isDoubleCross= true;
				int x=numRows-1;
				while(x<graphSize)
				{
					if(vert==x)
					{
						isDoubleCross=false;
						x=graphSize;
					}
					x+=(numRows);
					
				}
			}
		
		return isDoubleCross;
		
	}
	private boolean isNotEdgeString(int startV,int endV)
	{
		boolean isDoubleCross=false;
		int graphSize=numRows*numCol;
		if(startV!=numRows-1 && !(startV<=graphSize-1 && startV>=((numRows)*(numCol-1)))&& !(startV%(numRows)==0) && !(startV<numRows-1) )
		{
			if(endV!=numRows-1 && !(endV<=graphSize-1 && endV>=((numRows)*(numCol-1)))&& !(endV%(numRows)==0) && !(endV<numRows-1) )
			{
				
				
				isDoubleCross= true;
				int x=numRows-1;
				while(x<graphSize)
				{
					if(startV==x || endV==x)
					{
						isDoubleCross=false;
						x=graphSize;
					}
					x+=(numRows);
					
				}
			}
		}
		return isDoubleCross;
	}
	public int countCycles()
	{
		int count=0;
		List numDouble=bg.traverse(g);
		for(int i=0;i<numDouble.size();i++)
		{
			List temp=(List) numDouble.get(i);
			if(temp.size()>=4)
			{
				List pastVerts=new ArrayList();
				pastVerts.add((int)temp.get(0));
				if(isCyle((int)temp.get(0),(int)temp.get(0),pastVerts))
				{
					count++;
				}
			}
		}
		return count;
	}
	private boolean isCyle(int inVert,int previousVert,List<Integer> origin)
	{
		boolean chain=false;
	
			if(g.numEdges(inVert)==2)
			{
				int index=g.first(inVert);
				
				if(index==previousVert)
				{
					index=g.next(inVert, previousVert);
				}
				boolean ischain=false;
				for(int i=0;i<origin.size();i++)
				{
					if(index==origin.get(i))
					{
						ischain=true;
					}
				}
					
					
					if(ischain)
					{
						chain=true;
					}
					else
					{
						origin.add(index);
						chain=isCyle(index,inVert,origin);
					}
				
			}
			return chain;
	}
	private void isChain(int inVert,int parent,int nCount)
	{
		
		
			g.setMark(inVert, 1);
			int index=g.first(inVert);
			boolean end =true;
			if(g.degree(inVert)>2)
			{
				
				while(g.degree(index)!=2 && end)
				{
					index=g.next(inVert, index);
					if(index==g.vCount())
					{
						end=false;
						index=0;
					}
				}
			}
			else if(index==parent)
			{
				index=g.next(inVert, index);
			}
			if(end)
			{
				if(index!=g.vCount())
				{
					if((g.degree(index)==1||g.degree(index)>=3)&&nCount>2)
					{
						chainCount++;
					}
					
					else if(g.degree(index)==2)
					{
						
						if(g.getMark(index)==0)
						{
							isChain(index,inVert,(nCount+1));
						}
							
						
					}
					else
					{
						g.setMark(index, 1);
					}
					
						
				}
				
			}

	}
	public int countOpenChains()
	{
		chainCount=0;
		int count=0;
		List needed=new ArrayList();
				for(int i=0;i<g.vCount();i++)
				{
					if(!isNotEdgeVerteg(i)||g.degree(i)>2)
					{
						
							needed.add(i);
						
					}
				}
				for(int i=0;i<needed.size();i++)
				{
					
					if(g.getMark((int)needed.get(i))==0)
					{
						isChain((int)needed.get(i),(int)needed.get(i),0);
						
					}
				}
				g.resetMarks();
		return chainCount;
	}
	
	
}
