import java.util.ArrayList;
import java.util.List;


public class BfsGraphTraversal {
	private int count;
	List<List<Integer>> traverse(Graph g)
	{
		List mainList=new ArrayList();
		
		count=0;
		for(int i=0;i<g.vCount();i++)
		{
			if(g.getMark(i)==0)
			{
				List innerList=new ArrayList();
				count+=1;
				g.setMark(i, count);
				innerList.add(i);
				bfs(i,innerList,g);
				mainList.add(innerList);
			}
		}
		for(int i=0;i<g.vCount();i++)
		{
			g.setMark(i, 0);
		}
		return mainList;
	}
	private void bfs(int v,List current,Graph g)
	{
		
		
		int index=0;
		while(index<g.vCount())
		{
			index=g.next(v, index);
			if(index<g.vCount())
			{
				if(g.getMark(index)==0)
				{
					count+=1;
					if(!current.contains(index))
					{
						current.add(index);
					}
				}
			}
		}
		index=0;
		while(index<g.vCount())
		{
			index=g.next(v, index);
			if(index<g.vCount())
			{
				if(g.getMark(index)==0)
				{
					g.setMark(index, count);
						bfs(index,current,g);
				}
				
			}
		}
		
		
	}
	
	
}
