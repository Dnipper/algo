import java.util.ArrayList;
import java.util.List;


public class DfsGraphTraversal {
	private int count;
	List<List<Integer>> traverse(Graph g)
	{
		List mainList=new ArrayList();
		count=0;
		for(int i=0;i<g.vCount();i++)
		{
			
			if(g.getMark(i)==0)
			{
				List innerList=new ArrayList();
				dfs(i,innerList,g);
				mainList.add(innerList);
			}
			
		}
		for(int i=0;i<g.vCount();i++)
		{
			g.setMark(i, 0);
		}
		return mainList;
	}
	private void dfs(int v,List current,Graph g)
	{
		count+=1;
		if(v<g.vCount())
		{
			g.setMark(v, count);
			current.add(v);
			for(int j=0;j<g.vCount();j++)
			{
				int notCount=g.next(v, j);
				if(notCount<g.vCount())
				{
					int markIndex=g.getMark(notCount);
					if(markIndex==0)
					{
						dfs(notCount,current,g);
					}
				}
			}
		}
	}
	
}
