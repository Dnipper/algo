
public class HuffmanNode<T> implements Comparable<HuffmanNode>  {
	public T data;
	public HuffmanNode left;
	public HuffmanNode right;
	public String codeContains;
	Float frequency;
	@Override
	public int compareTo(HuffmanNode inNode) {
		int result=0;
		
		result=this.frequency.compareTo(inNode.frequency);
		return result;
	}
	public boolean isLeaf()
	{
		return(this.left==null&&this.right==null);
	}

}
