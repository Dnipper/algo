import java.nio.charset.Charset;
import java.util.PriorityQueue;

import edu.neumont.io.Bits;


public class HuffmanTree {
	private PriorityQueue pq=new PriorityQueue();
	private HuffmanNode hnRoot=new HuffmanNode();
	byte[] allBytes=new byte[256];
	public HuffmanTree(byte[] b)
	{
		setUpArray();
		makeHuffmanTree(b);
	}
	private void makeHuffmanTree(byte[] b)
	{
		
		for(int i=0;i<256;i++)
		{
			HuffmanNode temp=new HuffmanNode();
			int count=0;
			for(int j=0;j<b.length;j++)
			{
				if(allBytes[i]==b[j])
				{
					count++;
				}
			}
			if(count>0)
			{
				temp.data=allBytes[i];
				temp.frequency=(float)count/b.length;
				temp.codeContains=""+allBytes[i];
				pq.add(temp);
			}
		}
		
		while(pq.size()>1)
		{
			HuffmanNode temp2=new HuffmanNode();
			HuffmanNode temp3=new HuffmanNode();
			HuffmanNode parent=new HuffmanNode();
			
			temp2=(HuffmanNode) pq.poll();
			temp3=(HuffmanNode) pq.poll();
			parent.left=temp2;
			parent.right=temp3;
			parent.frequency=temp2.frequency+temp3.frequency;
			parent.codeContains=temp2.codeContains+" or "+ temp3.codeContains;
			pq.add(parent);
			
		}
		hnRoot=(HuffmanNode) pq.poll();
		
	}
	private void setUpArray()
	{
		byte x=-128;
		for(int i=0;i<256;i++)
		{
			allBytes[i]=x;
			x++;
		}
	}
	public void printTree()
	{
		helperPrintTree(hnRoot,null);
	}
	private void helperPrintTree(HuffmanNode printNode,HuffmanNode parent)
	{
		if(parent!=null)
		{
			if(printNode.isLeaf())
			{
				System.out.println("Data: "+ printNode.data+"," + " Frequency: " + printNode.frequency+","+ " Parent: " +parent.codeContains);
			}
			else
			{
				System.out.println("Data: "+ printNode.codeContains+"," + " Frequency: " + printNode.frequency+","+ " Parent: " +parent.codeContains);
			}
		}
		else
		{
			System.out.println("Data: "+ printNode.codeContains+"," + " Frequency: " + printNode.frequency);
		}
		if(!printNode.isLeaf())
		{
			helperPrintTree(printNode.left,printNode);
			helperPrintTree(printNode.right,printNode);
		}
	}
	public  byte toByte(Bits bits)
	{
		
		return helpertoByte(hnRoot,bits);
		
	}
	public byte helpertoByte(HuffmanNode inNode,Bits bits)
	{
		byte returnByte =0;
		if(inNode.isLeaf())
		{
			return returnByte=(byte) inNode.data;
		}
		if(bits.size()==0)
		{
			throw new IllegalArgumentException("Data cannot be found");
		}
		if(!bits.poll())
		{
			returnByte=helpertoByte(inNode.left,bits);
		}
		else
		{
			returnByte=helpertoByte(inNode.right,bits);
		}
		return returnByte;
		
	}
	public void fromByte(byte b, Bits bits)
	{
		fromByteHelper(b,bits,hnRoot);
	}
	
	public void fromByteHelper(byte b, Bits bits,HuffmanNode inNode)
	{
		if(!inNode.isLeaf())
		{
		String[] something=inNode.codeContains.split(" or ");
		byte[] mebytes=new byte[something.length];
		boolean foundNumb=false;
		for(int i=0;i<something.length;i++)
		{
			mebytes[i]= Byte.parseByte(something[i]);
		}
		for(int i=0;i<(mebytes.length);i++)
		{
			if(b==mebytes[i])
			{
				foundNumb=true;
			}
		}
		if(foundNumb)
		{
			boolean numerIsinLeft=false;
			String[] something1=inNode.left.codeContains.split(" or ");
			
			byte[] mebytes1=new byte[something1.length];
		
			for(int i=0;i<something1.length;i++)
			{
				mebytes1[i]= Byte.parseByte(something1[i]);
			}
			
			
			for(int i=0;i<(mebytes1.length);i++)
			{
				if(mebytes1[i]==b)
				{
					numerIsinLeft=true;
				}
			}
			
				if(numerIsinLeft)
				{
					bits.add(false);
					fromByteHelper(b, bits,inNode.left);
				}
				else
				{
					bits.add(true);
					fromByteHelper(b, bits,inNode.right);
				}
			
			}
		}
	}
}
