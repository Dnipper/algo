import edu.neumont.io.Bits;


public class HuffmanCompressor  {
	
	public byte[] compress (HuffmanTree tree, byte[] b) {
		
		
		Bits bit=new Bits();
		for(int i=0;i<b.length;i++)
		{
			tree.fromByte(b[i], bit);
		}
		String s="";
			while(bit.size()>0)
			{
				if(bit.poll())
				{
					s+="1";
							
				}
				else
				{
					s+="0";
				}
			}
		int sLen = s.length();
	    byte[] toReturn = new byte[(sLen + Byte.SIZE - 1) / Byte.SIZE];
	    char c;
	    for( int i = 0; i < sLen; i++ )
	    {
	        if( (c = s.charAt(i)) == '1' )
	        {
	            toReturn[i / Byte.SIZE] = (byte) (toReturn[i / Byte.SIZE] | (0x80 >>> (i % Byte.SIZE)));
	        }
		
	    }
		return toReturn;
		
		
		
	}
	public byte[] decompress(HuffmanTree tree, int uncompressedLength, byte[] b) {
		
		StringBuilder s = new StringBuilder(b.length * Byte.SIZE);
	    for( int i = 0; i < Byte.SIZE * b.length; i++ )
	    {
	        s.append((b[i / Byte.SIZE] << i % Byte.SIZE & 0x80) == 0 ? '0' : '1');
	    }
	    Bits bit= new Bits();
	    for(int i=0;i<s.length();i++)
	    {
	    	if(s.charAt(i)=='1')
	    	{
	    		bit.add(true);
	    	}
	    	else
	    	{
	    		bit.add(false);
	    	}
	    }
	    byte[] meByte=new byte[uncompressedLength];
	    int index=0;
	    while(bit.size()>0&& index<meByte.length)
		{
	    	meByte[index]=	tree.toByte(bit);
	    	index++;
				}
		return meByte;
	}


}
