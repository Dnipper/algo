import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.neumont.util.Queue;



public class LinkedList<T> implements Queue<T> {
	public Node<T> head;
	public Node<T> tail;
	private int size;
	public LinkedList()
	{
		
		head=new Node<T>(null);
	}
	public Iterator<T> iterator()
	{
		CustomIterator<T> cl=new CustomIterator<T>(this);
		cl.list=this;
		
		return cl;
		
	}
	public T poll()
	{
		if(size==0)
		{
			throw new IllegalArgumentException("The List is empty");
		}
		else
		{
			
			T temp=(T) head.data;
			head.next=head.next.next;
			return temp;
			
		}
		
	}
	public boolean offer(T t)
	{
		if(t==null)
		{
			throw new IllegalArgumentException("Data cannot be null");
		}
		if(head.next==null)
		{
			head.next=new Node<T>(t);
			tail=head.next;
		}
		else
		{
			tail.next=new Node<T>(t);
			tail=tail.next;
		}
		size++;
		return true;
		
	}
	public int getSize()
	{
		return size;
	}
	public T peek()
	{
		return (T) head.next.data;
	}
	public class CustomIterator<T>  implements Iterator<T>
	{ 
	    protected LinkedList<T> list;      
	    protected Node<T> pointer; 
	    protected Node<T> previous;  
	     
	    
	    public CustomIterator(LinkedList<T> inList)
	    { 
	    	list=inList;
	        pointer = list.head; 
	        previous = null; 
	    } 
	     
	     
	    
	    public boolean hasNext() 
	    { 
	        if (pointer == null) 
	           return (head != null);   
	        else 
	           return (pointer.next != null); 
	         
	    } 
	     
	   
	    public T next() throws NoSuchElementException  
	    { 
	        if (!hasNext())  
	            throw new NoSuchElementException();  
	        else {  
	            previous = pointer;  
	            pointer = pointer.next;  
	            return pointer.data;  
	        } 
	    }



		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		} 
	} 
	
}

