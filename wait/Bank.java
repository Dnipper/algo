import java.util.Iterator;

import edu.neumont.util.Client;
import edu.neumont.util.QueueableService;


public class Bank implements QueueableService {
	ArrayList<BankClient> clientsBeingServed= new ArrayList<BankClient>();
	ArrayList<BankClient> clients=new ArrayList<BankClient>();
	int tellers=0;
	int index=0;
	public Bank(int numberOfTellers) {
		tellers=numberOfTellers;
	}
	public void advanceMinute() {
		for(int i=0;i<clientsBeingServed.size();i++)
		{
			clientsBeingServed.get(i).c.servedMinute();
			
		}
		for(int i=0;i<clients.size();i++)
		{
			clients.get(i).waitTime-=1;
			if(clients.get(i).waitTime<=0)
			{
				clientsBeingServed.addAT(clients.get(i).teller, clients.get(i));
				clients.remove(clients.get(i));
			}
			
		}
		
	}
	
	
	public double getAverageClientWaitTime() {
		double times=0;
		for(int i=0;i<clients.size();i++)
		{
			
				times+=	clients.get(i).waitTime;
			
		}
		times=times/clients.size();
		return times;
	}
	@Override
	public double getClientWaitTime(Client client) {
		double returnTime=-1;
		
		for(int i=0;i<tellers;i++)
		{
			if(clientsBeingServed.get(i).c==client)
			{
				returnTime=0;
			}
		}
		if(returnTime==-1)
		{
			for(int i=0;i<clients.size();i++)
			{
				if(clients.get(i).c==client)
				{
					returnTime=clients.get(i).waitTime;
				}
			}
		}
		
		return returnTime;
	}
	@Override
	public boolean addClient(Client client) {
		
		BankClient temp =new BankClient();
		temp.c=client;
		if( index< tellers)
		{
			temp.waitTime=0;
			temp.teller=index;
			clientsBeingServed.add(temp);
			
			index++;
					
		}
		else
		{
			double timeHolder=0;
			for(int i=0;i<tellers;i++)
			{
				if(i==0)
				{
					double test=CalculateWiatTime(i);
					temp.waitTime=CalculateWiatTime(i);
					temp.teller=i;
				}
				else
				{
					timeHolder=CalculateWiatTime(i);
					if(timeHolder<temp.waitTime)
					{
						temp.waitTime=timeHolder;
						temp.teller=i;
					}
				}
				
			}
			clients.add(temp);
		}
		
		return true;
	}
	private double CalculateWiatTime(int teller)
	{
		double returnDouble=0;
		returnDouble+=clientsBeingServed.get(teller).c.getExpectedServiceTime();
		
		for(int i=0;i<clients.size();i++)
		{
			if(clients.get(i).teller==teller)
			{
				returnDouble+=clients.get(i).c.getExpectedServiceTime();
			}
		}
		return returnDouble;
	
	}
}
