import java.util.Iterator;

import edu.neumont.util.Client;
import edu.neumont.util.QueueableService;


public class GroceryStore implements QueueableService {
	
	ArrayList<LinkedList> aisle= new ArrayList<LinkedList>();
	int numRegisters;
	int index=0;
	public GroceryStore(int numberOfRegesters) {
		numRegisters=numberOfRegesters;
		for(int i=0;i<numberOfRegesters;i++)
		{
			LinkedList temp=new LinkedList();
			aisle.add(temp);
		}
	}
	public void advanceMinute() {
		for(int i=0;i<aisle.size();i++)
		{
			
			
			Client temp =(Client) aisle.get(i).peek();
			temp.servedMinute();
			double timeLeft=temp.getExpectedServiceTime();
			if(timeLeft<=0)
			{
				aisle.get(i).poll();
			}
					
			
		}
	}
	public double getClientWaitTime(Client client) {
		double times=0;
		
		for(int i=0;i<aisle.size();i++)
		{
			Iterator temp=aisle.get(i).iterator();
			
			while(temp.hasNext())
			{
				Client temp2=((Client) temp.next());
				if(temp2==client)
				{
					i=aisle.size();
					return times;
				}
				else
				{
					times+=temp2.getExpectedServiceTime();
				}
				
			}
			times=0;
		}
		return times;
	}
	@Override
	public double getAverageClientWaitTime() {
		double times=0;
		int count=0;
		double total=0;
		for(int i=0;i<aisle.size();i++)
		{
			Iterator temp=aisle.get(i).iterator();
			
			while(temp.hasNext())
			{
				double temp2=((Client) temp.next()).getExpectedServiceTime();
				count++;
				if(temp.hasNext())
				{
					times+=temp2;
					
				}
			}
			
			total+=times/count;	
			times=0;
			count=0;
		}
		total=total/numRegisters;
		return total;
	}
	@Override
	public boolean addClient(Client client) {
		int size=0;
		int index=0;
		size=aisle.get(0).getSize();
		for(int i=0;i<aisle.size();i++)
		{
			if(aisle.get(i).getSize()<size)
			{
				index=i;
				size=aisle.get(i).getSize();
			}
		}
		
		aisle.get(index).offer(client);
		return true;
	}
}